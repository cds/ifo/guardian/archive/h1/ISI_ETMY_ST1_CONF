from .. import const as top_const
import const as wd_const


def read_dackill_state():
    if top_const.CHAMBER_TYPE == 'MANAGER':
        channel = 'ISI-'+top_const.CHAMBER+'_DACKILL_STATE'
    elif top_const.CHAMBER_TYPE in ['HAM', 'HPI']:
        channel = 'DACKILL_STATE'
    else:
        channel = ':'+top_const.CHAMBER_TYPE+'_'+top_const.CHAMBER+'_DACKILL_STATE'
    return ezca.read(channel)


def is_dackill_tripped():
    return read_dackill_state() == wd_const.DACKILL_TRIPPED_STATE


def read_watchdog_state():
    return ezca.read('WD_MON_STATE_INMON')


def is_watchdog_tripped():
    return not wd_const.WATCHDOG_ARMED_STATE == read_watchdog_state()
