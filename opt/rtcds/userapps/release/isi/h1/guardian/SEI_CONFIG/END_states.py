# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState, GuardStateDecorator, NodeManager
from ezca.ligofilter import LIGOFilter
from isiguardianlib.ligoblend import LIGOBlendManager
from isiguardianlib import decorators as dec
from isiguardianlib.blend import states as blend_states
from isiguardianlib.blend import const as blend_const
from isiguardianlib.sensor_correction import states as sc_states
from isiguardianlib.sensor_correction import const as sc_const
from isiguardianlib import const as top_const
import const
import re
######################################


SC_ramp_time = 5

# Determine which node it is, then initialize the node manager
BRS_node_dict = {'X':'BRSX_STAT', 'Y':'BRSY_STAT'}
try:
    end_station = re.search(r'((?<=ETM)[X,Y])', _SYSTEM_).group(1)
except AttributeError:
    raise '_SYSTEM_ name does not match re for BRS_STAT nodes'
BRS_STAT = BRS_node_dict[end_station]
nodes = NodeManager([BRS_STAT])

##################################################
# Configuration Control


class desired_conf():
    """This will make it a bit more clear what configuration and filter 
    we are calling when writing new states. The purpose was to allow for 
    easy state creation. Hopefully it makes it easier and not more 
    confusing..

    Sample use:
    >>> A = desired_conf('SC', 'A')
    >>> A.Filter_Banks
    ['WNR','FIR']
    >>> A.MATCH_X
    [2,3]
    """
    def __init__(self, BLENDorSC, conf_letter):
        if BLENDorSC == 'BLEND':
            self.blend = True
            self.SC = False
            self.filter_dict = const.BLEND_FILTER_SELECTION
        elif BLENDorSC == 'SC':
            self.SC = True
            self.blend = False
            self.filter_dict = const.SC_FM_confs
        self.conf = conf_letter
        # Run to set attributes
        self.bank_dof()
    @property
    def Filter_Banks(self):
        banks = [bank for bank in self.filter_dict[self.conf]]
        final_banks = []
        for bank in banks:
            total_dofs = []
            for dof in self.filter_dict[self.conf][bank]:
                if self.filter_dict[self.conf][bank][dof]:
                    total_dofs += dof
            if len(total_dofs) != 0:
                final_banks.append(bank)
        return final_banks
    @property
    def Blend_dofs(self):
        if self.blend:
            return [dof for dof in self.filter_dict[self.conf]['BLEND'] \
                        if self.filter_dict[self.conf]['BLEND'][dof]]
        else:
            return None
    @property
    def SC_dofs(self):
        if self.SC:
            SCdict = {}
            for bank in self.Filter_Banks:
                SCdict[bank] = [dof for dof in self.filter_dict[self.conf][bank] \
                        if self.filter_dict[self.conf][bank][dof]]
            return SCdict
        else:
            return None
    def bank_dof(self):
        for bank in self.Filter_Banks:
            for dof in self.filter_dict[self.conf][bank]:
                setattr(self, bank+'_'+dof, self.filter_dict[self.conf][bank][dof])

# Config assignments
blend_45 = desired_conf('BLEND', 'A')
blend_90 = desired_conf('BLEND', 'B')
blend_250 = desired_conf('BLEND', 'C')

windy_SC = desired_conf('SC', 'B')
usei_SC = desired_conf('SC', 'A')
eq_SC = desired_conf('SC', 'C')
##################################################
# Decorators

class verify_BRS(GuardStateDecorator):
    def pre_exec(self):
        if nodes[BRS_STAT].STATE != 'READY':
            notify('BRS is not READY')
            #return 'WAITING_FOR_BRS'

##################################################
# Gen States

def get_SC_switch_one_bank(bank, dofs, gain, ramp_time=5, requestable=False, BRS=False):
    class SC_switch_one_bank(GuardState):
        """Ramp ON the gain of the bank and its dof(s) before the MATCH bank
        and then ramp ON the MATCH bank.
        """
        request = requestable
        def main(self):
            if BRS:
                if nodes[BRS_STAT].STATE != 'READY':
                    # Jumping here will let the node wait until it is better and then INIT to 
                    # get back to where it should be.
                    return 'WAITING_FOR_BRS'
            for dof in dofs:
                # Ramp ON the filter gain
                lf_bank = LIGOFilter(sc_const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                lf_bank.ramp_gain(gain, ramp_time=ramp_time, wait=True)
                # Ramp ON the MATCH bank to allow the previous filter signal through
                lf_match = LIGOFilter(sc_const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank='MATCH'), ezca)
                lf_match.ramp_gain(gain, ramp_time=ramp_time, wait=True)
            return True
    return SC_switch_one_bank

##################################################
# States

class WAITING_FOR_BRS(GuardState):
    goto = True
    request = False
    def main(self):
        notify('Waiting for BRS to settle')
        # Rerequest Ready incase it somehow got out.
        nodes[BRS_STAT] = 'READY'
    def run(self):
        if nodes.arrived:
            # Jump to INIT so that it can then continue to where it should be
            return 'INIT'
        else:
            notify('Waiting for the BRS to settle')
            ### FIXME: How are people suppose to get out of this state if they 
            ### dont want to be here? 


class INIT(GuardState):
    """Check the status of the blends and SC to determine which state to go to"""
    def main(self):
        ##############################
        nodes.set_managed()
        # This assumes that it wont jump to READY when setting the request
        nodes[BRS_STAT] = 'READY'
        ##############################

        ### All of this is if we continue to change blends as well.

        self.lbm = LIGOBlendManager(deg_of_free_list=const.CHAMBER_BLEND_FILTERS_DOF, ezca=ezca)
        # A list of tuples containing the number and name respectively of the current filter
        blend_names = {dof : self.lbm.get_cur_blend_name(dof) for dof in const.CHAMBER_BLEND_FILTERS_DOF}

        ## Check if in Blend A
        # Make sure the dof sets are the same
        if set(blend_45.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_45, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_45.Blend_dofs):
                # We are in Blend A !!!

                ### FIXME: This will be a bit more simple for now and just look 
                ### at the gains of the banks until Jim is done chaning configs
                FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(end_station)]
                WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(end_station)]
                IIRHP_gain = ezca['SENSCOR_GND_STS_{}_IIRHP_GAIN'.format(end_station)]
                MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(end_station)]
                
                if FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 0.0:
                    return 'BLEND_45MHZ_SC_NONE'
                elif FIR_gain == 1.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_45MHZ_SC_BRS'
                elif FIR_gain == 0.0 and WNR_gain == 1.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_45MHZ_SC_USEISM'
                elif FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 1.0 and MATCH_gain == 1.0:
                    return 'BLEND_45MHZ_SC_EQ'                    
                else:
                    # I guess go here???
                    log('SC does not match any known configuration, going to BLEND_45mHz_SC_None')
                    return 'BLEND_45MHZ_SC_NONE'
            else:
                log('CPA')

        ## Check if in Blend B
        # Make sure the dof sets are the same
        if set(blend_90.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_90, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_90.Blend_dofs):
                # We are in Blend B !!!
                ### FIXME: This will be a bit more simple for now and just look 
                ### at the gains of the banks until Jim is done chaning configs
                FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(end_station)]
                WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(end_station)]
                IIRHP_gain = ezca['SENSCOR_GND_STS_{}_IIRHP_GAIN'.format(end_station)]
                MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(end_station)]

                if FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 0.0:
                    return 'BLEND_Quite_90_SC_None'
                elif FIR_gain == 1.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_Quite_90_SC_BRS'
                elif FIR_gain == 0.0 and WNR_gain == 1.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_Quite_90_SC_useism'
                elif FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 1.0 and MATCH_gain == 1.0:
                    return 'BLEND_Quite_90_SC_EQ'                    
                else:
                    # I guess go here???
                    log('SC does not match any known configuration, going to BLEND_Quite_90_SC_None')
                    return 'BLEND_Quite_90_SC_None'
            else:
                log('CPB')

        ## Check if in Blend C
        # Make sure the dof sets are the same
        if set(blend_250.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_250, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_250.Blend_dofs):
                # We are in Blend B !!!
                ### FIXME: This will be a bit more simple for now and just look 
                ### at the gains of the banks until Jim is done chaning configs
                FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(end_station)]
                WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(end_station)]
                IIRHP_gain = ezca['SENSCOR_GND_STS_{}_IIRHP_GAIN'.format(end_station)]
                MATCH_gain = ezca['SENSCOR_GND_STS_{}_MATCH_GAIN'.format(end_station)]
                
                if FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 0.0:
                    return 'BLEND_QUITE_250_SC_NONE'
                elif FIR_gain == 1.0 and WNR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_QUITE_250_SC_BRS'
                elif FIR_gain == 0.0 and WNR_gain == 1.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                    return 'BLEND_QUITE_250_SC_USEISM'
                elif FIR_gain == 0.0 and WNR_gain == 0.0 and IIRHP_gain == 1.0 and MATCH_gain == 1.0:
                    return 'BLEND_QUITE_250_SC_EQ'                    
                else:
                    # I guess go here???
                    log('SC does not match any known configuration, going to BLEND_Quite_250_SC_None')
                    return 'BLEND_QUITE_250_SC_NONE'
            else:
                log('CPC')
                
        else:
            log('Blends are not in a known configuration, going to DOWN')
            return 'DOWN'


####################
# Blend states

SWITCH_TO_QUITE_90_BLEND = blend_states.get_basic_blend_switch_state(
        deg_of_free_list=blend_90.Blend_dofs, 
        # Just grab the first FM# since they should be the same anyway
        blend_number=getattr(blend_90, 'BLEND_{}'.format(blend_90.Blend_dofs[0]))[0], 
        requestable=False)

SWITCH_TO_45MHZ_BLEND = blend_states.get_basic_blend_switch_state(
        deg_of_free_list=blend_45.Blend_dofs,
        # Just grab the first FM# since they should be the same anyway
        blend_number=getattr(blend_45, 'BLEND_{}'.format(blend_45.Blend_dofs[0]))[0], 
        requestable=False)

SWITCH_TO_QUITE_250_BLEND = blend_states.get_basic_blend_switch_state(
        deg_of_free_list=blend_250.Blend_dofs,
        # Just grab the first FM# since they should be the same anyway
        blend_number=getattr(blend_250, 'BLEND_{}'.format(blend_250.Blend_dofs[0]))[0], 
        requestable=False)

####################
# SC states

TURN_ON_BLEND_45_BRS_SC = get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5,
    BRS=True)
TURN_ON_BLEND_45_USEISM_SC = get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_45_EQ_SC = get_SC_switch_one_bank(
    eq_SC.Filter_Banks[0],
    eq_SC.SC_dofs[eq_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_90_BRS_SC = get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5,
    BRS=True)
TURN_ON_BLEND_90_USEISM_SC = get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_90_EQ_SC = get_SC_switch_one_bank(
    eq_SC.Filter_Banks[0],
    eq_SC.SC_dofs[eq_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_250_BRS_SC = get_SC_switch_one_bank(
    windy_SC.Filter_Banks[0],
    windy_SC.SC_dofs[windy_SC.Filter_Banks[0]],
    1,
    ramp_time=5,
    BRS=True)
TURN_ON_BLEND_250_USEISM_SC = get_SC_switch_one_bank(
    usei_SC.Filter_Banks[0],
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]],
    1,
    ramp_time=5)
TURN_ON_BLEND_250_EQ_SC = get_SC_switch_one_bank(
    eq_SC.Filter_Banks[0],
    eq_SC.SC_dofs[eq_SC.Filter_Banks[0]],
    1,
    ramp_time=5)



# Three of the same states but with different names to make a longer path so it doesn't skip
TURN_OFF_BLEND_90_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks + eq_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] \
    + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]] \
    + eq_SC.SC_dofs[eq_SC.Filter_Banks[0]], ### FIXME: This only works while we are just using one FB, need to find a more creative way to find the dofs
    ramp_time=5)
TURN_OFF_BLEND_45_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks + eq_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] \
    + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]] \
    + eq_SC.SC_dofs[eq_SC.Filter_Banks[0]], ### FIXME: see above
    ramp_time=5)
TURN_OFF_BLEND_250_SC = sc_states.get_SC_all_gains_off(
    usei_SC.Filter_Banks + windy_SC.Filter_Banks + eq_SC.Filter_Banks,
    usei_SC.SC_dofs[usei_SC.Filter_Banks[0]] \
    + windy_SC.SC_dofs[windy_SC.Filter_Banks[0]] \
    + eq_SC.SC_dofs[eq_SC.Filter_Banks[0]], ### FIXME: see above above
    ramp_time=5)

####################
# Idle states

##########
# 45s
class BLEND_45MHZ_SC_BRS(GuardState):
    index = 48
    @nodes.checker()
    @verify_BRS
    def main(self):
        return True
    @nodes.checker()
    @verify_BRS
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True

class BLEND_45MHZ_SC_NONE(GuardState):
    index = 45
    def main(self):
        return True
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True

class BLEND_45MHZ_SC_USEISM(GuardState):
    index = 47
    def main(self):
        return True
    def run(self):
        return True

class BLEND_45MHZ_SC_EQ(GuardState):
    index=49
    def main(self):
        return True
    def run(self):
        return True

##########
# 90s
class BLEND_QUITE_90_SC_NONE(GuardState):
    index = 90
    def main(self):
        return True
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True
    
class BLEND_QUITE_90_SC_USEISM(GuardState):
    index = 97
    def main(self):
        return True
    def run(self):
        return True

class BLEND_QUITE_90_SC_BRS(GuardState):
    index = 98
    @nodes.checker()
    @verify_BRS
    def main(self):
        return True
    @nodes.checker()
    @verify_BRS
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True

class BLEND_QUITE_90_SC_EQ(GuardState):
    index=99
    def main(self):
        return True
    def run(self):
        return True

###########
# 250s
class BLEND_QUITE_250_SC_NONE(GuardState):
    index = 250
    def main(self):
        return True
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True
    
class BLEND_QUITE_250_SC_USEISM(GuardState):
    index = 257
    def main(self):
        return True
    def run(self):
        return True

class BLEND_QUITE_250_SC_BRS(GuardState):
    index = 258
    @nodes.checker()
    @verify_BRS
    def main(self):
        return True
    @nodes.checker()
    @verify_BRS
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True

class BLEND_QUITE_250_SC_EQ(GuardState):
    index=259
    def main(self):
        return True
    def run(self):
        return True

##########
class DOWN(GuardState):
    """This state is more of just a parking spot for the node to go
    when someone wants to change the configuration to something other
    than what is defined here, or if there is a problem it can jump to DOWN.
    """
    goto = True
    index = 1
    def main(self):
        return True
    def run(self):
        if nodes[BRS_STAT].STALLED:
            nodes[BRS_STAT].revive()
        return True
##################################################

edges = [
    #####
    # BLEND CHANGES
    ('BLEND_45MHZ_SC_NONE', 'SWITCH_TO_QUITE_90_BLEND'),
    ('BLEND_45MHZ_SC_NONE', 'SWITCH_TO_QUITE_250_BLEND'),
    ('SWITCH_TO_45MHZ_BLEND', 'BLEND_45MHZ_SC_NONE'),

    ('BLEND_QUITE_90_SC_NONE', 'SWITCH_TO_45MHZ_BLEND'),
    ('BLEND_QUITE_90_SC_NONE', 'SWITCH_TO_QUITE_250_BLEND'),
    ('SWITCH_TO_QUITE_90_BLEND', 'BLEND_QUITE_90_SC_NONE'),

    ('BLEND_QUITE_250_SC_NONE', 'SWITCH_TO_45MHZ_BLEND'),
    ('BLEND_QUITE_250_SC_NONE', 'SWITCH_TO_QUITE_90_BLEND'),
    ('SWITCH_TO_QUITE_250_BLEND', 'BLEND_QUITE_250_SC_NONE'),
    #####
    # 45 SC
    ('BLEND_45MHZ_SC_NONE', 'TURN_ON_BLEND_45_BRS_SC'),
    ('TURN_ON_BLEND_45_BRS_SC', 'BLEND_45MHZ_SC_BRS'),
    ('BLEND_45MHZ_SC_BRS', 'TURN_OFF_BLEND_45_SC'),

    ('BLEND_45MHZ_SC_NONE', 'TURN_ON_BLEND_45_USEISM_SC'),
    ('TURN_ON_BLEND_45_USEISM_SC', 'BLEND_45MHZ_SC_USEISM'),
    ('BLEND_45MHZ_SC_USEISM', 'TURN_OFF_BLEND_45_SC'),

    ('BLEND_45MHZ_SC_NONE', 'TURN_ON_BLEND_45_EQ_SC'),
    ('TURN_ON_BLEND_45_EQ_SC', 'BLEND_45MHZ_SC_EQ'),
    ('BLEND_45MHZ_SC_EQ', 'TURN_OFF_BLEND_45_SC'),

    ('TURN_OFF_BLEND_45_SC', 'BLEND_45MHZ_SC_NONE'),
    #####
    # 90 SC
    ('BLEND_QUITE_90_SC_NONE', 'TURN_ON_BLEND_90_BRS_SC'),
    ('TURN_ON_BLEND_90_BRS_SC', 'BLEND_QUITE_90_SC_BRS'),
    ('BLEND_QUITE_90_SC_BRS', 'TURN_OFF_BLEND_90_SC'),

    ('BLEND_QUITE_90_SC_NONE', 'TURN_ON_BLEND_90_USEISM_SC'),
    ('TURN_ON_BLEND_90_USEISM_SC', 'BLEND_QUITE_90_SC_USEISM'),
    ('BLEND_QUITE_90_SC_USEISM', 'TURN_OFF_BLEND_90_SC'),

    ('BLEND_QUITE_90_SC_NONE', 'TURN_ON_BLEND_90_EQ_SC'),
    ('TURN_ON_BLEND_90_EQ_SC', 'BLEND_QUITE_90_SC_EQ'),
    ('BLEND_QUITE_90_SC_EQ', 'TURN_OFF_BLEND_90_SC'),

    ('TURN_OFF_BLEND_90_SC', 'BLEND_QUITE_90_SC_NONE'),
    #####
    # 250 SC
    ('BLEND_QUITE_250_SC_NONE', 'TURN_ON_BLEND_250_BRS_SC'),
    ('TURN_ON_BLEND_250_BRS_SC', 'BLEND_QUITE_250_SC_BRS'),
    ('BLEND_QUITE_250_SC_BRS', 'TURN_OFF_BLEND_250_SC'),

    ('BLEND_QUITE_250_SC_NONE', 'TURN_ON_BLEND_250_USEISM_SC'),
    ('TURN_ON_BLEND_250_USEISM_SC', 'BLEND_QUITE_250_SC_USEISM'),
    ('BLEND_QUITE_250_SC_USEISM', 'TURN_OFF_BLEND_250_SC'),

    ('BLEND_QUITE_250_SC_NONE', 'TURN_ON_BLEND_250_EQ_SC'),
    ('TURN_ON_BLEND_250_EQ_SC', 'BLEND_QUITE_250_SC_EQ'),
    ('BLEND_QUITE_250_SC_EQ', 'TURN_OFF_BLEND_250_SC'),

    ('TURN_OFF_BLEND_250_SC', 'BLEND_QUITE_250_SC_NONE'),
    ]
